module bitbucket.org/lygo/lygo_resources

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	github.com/google/uuid v1.3.0 // indirect
)
